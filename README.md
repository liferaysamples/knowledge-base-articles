# Knowledge Base Articles

This is a boilerplate project so you can easily start your own knowledge base within Liferay where you manage the articles via code in your git repository.

To use the project, just clone it and read the article `src/04_technical/01_kdb_as_code.md` for the German verions or `src/04_technical/01_kdb_as_code_en.md` for the English one. There should be everything explained to get you started.

While reading the article I suggest you adapt all the settings according your environment - but this will be explained in the article.

## Prerequisites

You need following tools installed to build and deploy the project:

```
nodejs
npm
Liferay
a bit of Liferay Knowhow (eg. Sites concept,...)
```

Also if you checked out this project, you have to call an once before you can build

```
npm install
```

## Project structure

The project follows a very simple structure, all articles and the recources are in the `src` folder.

For a more detailled explanation please read the article mentioned above.

## Build

To build the wtwdb you have to call

```
npm run build
```

## Deployment

### Manual

If you ran the build successfully, you should have a dist folder with the file `knowledge-base-articles_1.0.0.zip` in it.

To deploy this file, login into your Liferay installation and set the Knowledge Base settings like described in the article. TODO describe here?

Afterwards, go to your site, then to `Contents & Data` -> `Knowledge Base` -> the blue `+` Button -> `Import`.

There you place the zip file, check that the checkbox is selected and press the `Save` Button.

Et voila, you have imported your first few knowledge base articles. Now place them onto a page and have fun getting all your knowledge written down in a scructured way.

### Automatic

TODO MM TBD: We need some service on liferay end to fetch the zip file and start the import process of the internal knowledge database.

## Development

### Create a new article

TODO MM

### Translation

To translate an article, just copy the file and rename it so it ends with the ID `_LANG_SHORT.md` eg. `01_my_article_en.md`.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Authors

* **[Manuel Manhart](https://www.manhart.space/)** - *Initial work*

## Contribution

No contributing since this is a sample project.

## Versioning

No versioning since this is a sample project.
